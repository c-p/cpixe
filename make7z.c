
/***************************************************************************
    Copyright (C) 2007 by Carlos Pascual-Izarra
    carlos.pascual_AT_users.sourceforge.net                                                  *

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 ***************************************************************************/
/**
Small hack to build a program that builds the distribution file.
It assumes that 7-zip is installed and in the system PATH
*/
#include <stdlib.h>
#include <stdio.h>
#include "version.h"

int main(){
char command[300];
sprintf(command,"7z a -t7z ../cpixe-%s.7z @distribution.list",FULLVERSION_STRING);
printf("%s",command);
system( command );
return 0;
}
