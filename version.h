#ifndef VERSION_H
#define VERSION_H

	//Software Status
	static const char STATUS[] =  "Beta";
	static const char STATUS_SHORT[] =  "b";
	
	//Standard Version Type
	static const long MAJOR  = 0;
	static const long MINOR  = 4;
	static const long BUILD  = 2;
	static const long REVISION  = 0;
	
	//Miscellaneous Version Types
	static const long BUILDS_COUNT  = 4;
	#define RC_FILEVERSION 0,4,2,0
	#define RC_FILEVERSION_STRING "0, 4, 2, 0\0"
	static const char FULLVERSION_STRING [] = "0.4.2.0";
	
	//These values are to keep track of your versioning state, don't modify them.
	static const long BUILD_HISTORY  = 0;
	

#endif //VERSION_H
