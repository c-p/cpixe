
/***************************************************************************
    Copyright (C) 2007 by Carlos Pascual-Izarra
    carlos.pascual_AT_users.sourceforge.net                                                  *

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 ***************************************************************************/

/**\file cpixe.c
CPIXE-EFF is a small program for calculating efficiency curves from a detector.

See the libcpixe.c file for more information.

*/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "../libcpixe/libcpixe.h"
#include "../libcpixe/compilopt.h"

//This overrides the setting in compilopt!
#define CPIXEVERBOSITY 0

#define OUTPUTABLE 1
#define OUTPUTBLOCK 1

int main(int argc, char *argv[]);
/**Main program:
 *
 * @return EXIT_SUCCESS if normal finishing
 */
int main(int argc, char *argv[])
{
    STATFILENAME crystalfilename="detcrystal.det";
    STATFILENAME windowfilename="detwindow.det";
    STATFILENAME abscoeffilename="xabs.abs";
    STATFILENAME xenerfilename="xener.dat";
    TwoCol *EffTable;
    CalibYld *EffBlockArray;
    double factor=1.;
    int i,maxZ,dimEffTable;

    if (argc>1)strcpy(crystalfilename,argv[1]);
    if (argc>2)strcpy(windowfilename,argv[2]);
    if (argc>3)strcpy(abscoeffilename,argv[3]);
    if (argc>4)strcpy(xenerfilename,argv[4]);

    maxZ=CreateEff(abscoeffilename, xenerfilename, windowfilename,crystalfilename, factor ,&EffBlockArray, &dimEffTable, &EffTable);

#if OUTPUTABLE > 1
    for (i=0; i<dimEffTable;i++)printf("\n%le\t%le",EffTable[i].x,EffTable[i].y);
    fprintf(stdout,"\n");
#endif
#if OUTPUTBLOCK > 1
    for (i=0; i<maxZ;i++)
    {
        fprintf(stdout,"\n%d\t%2s:", i,ChemicalSymbol[i]);
        fprintf(stdout,"\n");
        fprintCALIBYLD(stdout,&EffBlockArray[i]);
        fprintf(stdout,"\n");
    }
#endif

    free(EffBlockArray);
    free(EffTable);

    return EXIT_SUCCESS;
}


