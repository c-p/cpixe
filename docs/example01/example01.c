/*
Example01: How to retrieve an absortion energy.
By Carlos Pascual-Izarra

To compile, use the codeblock project.
...or do it manually, linking with "libcpixe.a"
*/
#include <stdio.h>
#include <stdlib.h>
#include "../../libcpixe/libcpixe.h"
int main()
{
    char abscoeffilename[300]="../../databases/xabs.abs";
    int maxZ=92;
    AbsCoef *TotAbsCoefArray;
    AbsCoef ac;
    double E;

    readAbsCoef(abscoeffilename, maxZ, &TotAbsCoefArray); //The readAbsCoef function from libcpixe

    ac=TotAbsCoefArray[55]; //let's say that we are interested in the element with Z=55
    E=ac.enr[3];  //the LII absorption line is the 3rd element of the enr member
    printf("\n\n\nEnerLII=%lf\n\n",E);

    //Now a compact form for retrieving the abs coef for the K edge of Si:
    E=TotAbsCoefArray[14].enr[1];
    printf("E K=%lf\n\n",E);
    return 0;
}
