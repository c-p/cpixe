
This directory contains some examples of input files that can be used to run the cpixe program.

- parameter input files (.in)
- sample definition files (.sdf) 
- filter definition files (.fdf) 
- detector efficiency (calibration) files (.eff)
- calculation flags files (.flg)

It also contains some output files (.cpf) resulting from running cpixe on the provided input files.

Note the given files assume that `cpixe` is run from its base directory (the parent of `testdata`). If you want to run it from somewhere else you will need to modify the .in files accordingly.

For example, you can run:

```console
bin/cpixe testdata/_01_cpixe.in
```

... and it should re-create the [testdata/_01_output.cpf](testdata/_01_output.cpf)