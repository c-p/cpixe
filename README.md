cpixe
=====

See http://cpixe.sourceforge.net/ for information about cpixe

To compile cpixe:

- Download the cpixe code from this repo
- Download the [codeblocks IDE](http://codeblocks.org) and install it
- Edit the libcpixe/compilopt.h file if needed (This is likely to be needed to compile on windows)
- Go to the cpixe source root dir and run: `codeblocks cpixe.cbp --build --target="everything"`

This should compile the library and also create some executables in the [bin](bin) directory. 

See [testdata/README.md](testdata) for a guide on how to test with provided examples.
