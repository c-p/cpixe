
/***************************************************************************
    Copyright (C) 2007 by Carlos Pascual-Izarra
    carlos.pascual_AT_users.sourceforge.net                                                  *

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 ***************************************************************************/

/**\file libcpixe/compilopt.h
In this file, some compilation options can be defined for:
a) tricks for solving the compilation and linking under the different plattforms or with different compilers.

This is not very elegant (and for sure is reinventing the wheel) but works for :
     Compiling with gcc under GNU/linux
     Compiling with MingW (gcc under Win)
     Linking with the DEC Visual Fortran

b) Select the verbosity level of the routines.
*/


/*Uncomment the following line if the strcasecmp() function
is not available (e.g. when linking with the DEC fortran linker under Win )
WARNING: This will change the behaviour from Non-case sensitiviness
to case-sensitiviness in various comparisons.
*/
//#define STRNCASECMPNOTAVAIL

#ifdef STRNCASECMPNOTAVAIL
  #define strcasecmp strcmp
  #define strncasecmp strncmp
#endif

/*Uncomment the following line if you get an "unresolved external" when linking to the
snprintf function (e.g. it hapens in WIN32 systems)*/
// #define SNPRINTFNOTAVAIL

#ifdef SNPRINTFNOTAVAIL
  #define snprintf        _snprintf
#endif

/*Uncomment following line if having trouble with sqrtf, pow, expf logf
(e.g., it happens when linking with DEC Fortran compiler)*/
// #define MATHFNOTAVAIL

#ifdef MATHFNOTAVAIL
       #define sqrtf sqrt
       #define logf log
       #define expf exp
       #define powf pow
#endif

/* VERBOSITY LEVEL of CPIXE and LibCPIXE. For efficiency, compile it with CPIXEVERBOSITY=0:
     0 = Silent  (Use for maximum efficiency when you are interested in using LibCPIXE as a library with another program)
     1 = Standard  (Use for debugging LibCPIXE or for compiling CPIXE )
     2 = Extra verbose (Use only in developement situations)
*/
#define CPIXEVERBOSITY 0

#define LibCPVERBOSITY 0

