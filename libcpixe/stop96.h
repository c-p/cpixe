
/***************************************************************************
    Copyright (C) 2007 by Carlos Pascual-Izarra
    carlos.pascual_AT_users.sourceforge.net

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

 ***************************************************************************/

/**\file stop96.h
 stop96.h is the header file for using stop96.c .It has been modified from the original version found in the hotstop code ( http://hotstop.sourceforge.net )
*/

#ifndef STP96C
float scoef[94][55];
void readstopcoef(char *afilename, char *bfilename);
float pstop(int Z2, float E);
void stop96(int Z1, int Z2, float M1, float *Ener, int nEner, float *SE);
void stop96d(int Z1, int Z2,double Ionweight, double *Ener, int nEner, double *SE);
float pstop_fit(int Z2,float E,float P0,float P1,float P2,float P3,float P4,float P5,float P6,float P7,float HE0,float HE1,float HE2,float HE3);
double nuclearstopping_ZBL(int Z1, int Z2,double Ionweight, double trgtweight, double *Ener, int nEner, double *SN);
#define STP96C 1
#endif


