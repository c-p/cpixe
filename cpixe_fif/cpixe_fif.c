/***************************************************************************
 *   Copyright (C) 2005-2018 by Carlos Pascual-Izarra                      *
 *   cpascual@cells.es                                                     *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/



/**\file cpixe_fif.c
This is a fortran interface, not properly part of the LibCPIXE lybrary.

I wrote it to facilitate the integration with the NDF program by N.P. Barradas but it might be useful as well in the case that other people wants to link libCPIXE to a Fortran program.

I try to maintain cpixe_fif.c with at least the same funcionalities available in the cpixe.c code.


*/


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "../libcpixe/libcpixe.h"
#include "../libcpixe/compilopt.h"





//Function prototypes:
int translatesample(int NFoil, int Nels, const int *pFOILZ,
                    const float *pFOILM, const float *pAMNT, const float *pFOILTHICK,
					int *MaxZinsample, foil **Sample);

int translatesample2(int NFoil, int MaxNels, const int *pNels, const int *pFOILZ,
                    const float *pFOILM, const float *pAMNT, const float *pFOILTHICK,
					int *MaxZinsample, foil **Sample);

int translateresults(int dimSums, int dimResults, const int *PresentElems,
                     const CalibYld *XYldSums, CPIXERESULTS *pRESULTS);

int translateresultsNDF(int Nels, const int *pFOILZ, const CalibYld *XYldSums, CPIXERESULTS *pRESULTS);

//Functions:

/*Translation of sample structures. */
int translatesample(int NFoil, int Nels, const int *pFOILZ,
                    const float *pFOILM, const float *pAMNT, const float *pFOILTHICK,
					int *MaxZinsample, foil **Sample)
{
  int i,j,nelem,iels;
  double sumM;
  foil *pfoil;
  COMPOUND *c;

  /*Initialize  Sample*/
  *Sample=(foil*)malloc((NFoil)*sizeof(foil));
  if (*Sample==NULL){printf("\n Error allocating memory (Sample)\n");exit(1);}
  for (*MaxZinsample=0,i = 0; i < NFoil ; i++) {

    //obtain the number of elements for this layer
    for(nelem=0, iels=0; iels<Nels ; iels++) if(pAMNT[iels+Nels*i]>0.) nelem++;  //CHECK THIS!!!
    if(nelem<1){fprintf(stderr,"\n ERROR: At least 1 element in each foil needed\n");exit(1);}

    pfoil=&((*Sample)[i]);
	  pfoil->nfoilelm=nelem;
	  pfoil->thick=(double)pFOILTHICK[i];
	  c=&pfoil->comp;

    c->nelem=nelem;
    if(!(c->elem=(ELEMENT*)calloc(nelem,sizeof(ELEMENT)))){fprintf(stderr,"\n Error allocating memory\n");exit(1);}
    if(!(c->X=(double*)calloc(nelem,sizeof(double)))){fprintf(stderr,"\n Error allocating memory\n");exit(1);}
    if(!(c->xn=(double*)calloc(nelem,sizeof(double)))){fprintf(stderr,"\n Error allocating memory\n");exit(1);}
    if(!(c->w=(double*)calloc(nelem,sizeof(double)))){fprintf(stderr,"\n Error allocating memory\n");exit(1);}
    for(j=0, c->sumX=0., sumM=0., iels=0 ; iels<Nels ; iels++){
      if(pAMNT[iels+Nels*i]>0.){
        c->X[j]= (double) (pAMNT[iels+Nels*i]);  //NOTE!!: The indexes are switched and "offseted" to deal with the the Fortran convention!!
	      c->elem[j].Z=pFOILZ[iels];
        c->elem[j].A=Z2mass(c->elem[j].Z, &c->elem[j].IM, 'm');
        c->elem[j].M=(double)pFOILM[iels];  //The mass passed by NDF is put on the "natural mass"
        c->sumX+=c->X[j];
        sumM+=c->elem[j].M*c->X[j];
        if(c->elem[j].Z>*MaxZinsample) *MaxZinsample=c->elem[j].Z;
        j++;
      }
    }
    /*Normalize concentrations and calculate the MASS concentration*/
    for(j=0;j<nelem;j++){
      c->xn[j]=c->X[j]/c->sumX;
      c->w[j]=c->X[j]*c->elem[j].M/sumM;
    }
  }
  #if CPIXEVERBOSITY>3
  for(i=0;i<NFoil;i++){
     printf("\nFoil %d: th=%le nelem=%d",i,(*Sample)[i].thick,(*Sample)[i].nfoilelm);
     for(j=0;j<(*Sample)[i].nfoilelm;j++){
       printf("\nTrans: (i=%d , j=%d) (*Sample)[i].comp.elem[j].Z=%d ",i,j,(*Sample)[i].comp.elem[j].Z);
       printf("\nTrans:             (*Sample)[i].comp.elem[j].M=%lf ",(*Sample)[i].comp.elem[j].M);
       printf("\nTrans:             (*Sample)[i].comp.elem[j].IM=%lf ",(*Sample)[i].comp.elem[j].IM);
       printf("\nTrans:             (*Sample)[i].comp.elem[j].A=%d ",(*Sample)[i].comp.elem[j].A);
       printf("\nTrans:      X=%lf   xn=%lf   w=%lf",(*Sample)[i].comp.X[j],(*Sample)[i].comp.xn[j],(*Sample)[i].comp.w[j]);
      }
    }
  #endif
  return(0);

}

/*Alternative translation of sample structures. */
int translatesample2(int NFoil, int MaxNels, const int *pNels, const int *pFOILZ,
                    const float *pFOILM, const float *pAMNT, const float *pFOILTHICK,
					int *MaxZinsample, foil **Sample)
{
  int i,j,nelem;
  double sumM;
  foil *pfoil;
  COMPOUND *c;

  #if CPIXEVERBOSITY >1
  printf("\n\nDEBUG:Translatesample2(): NFoil=%d    MaxNels=%d",NFoil,MaxNels);
  for(i=0;i<NFoil;i++){
    printf("\nDEBUG:Translatesample2(): Foil %d  Nels=%d",i,pNels[i]);
    for(j=0;j<pNels[i];j++)printf("\nDEBUG:Translatesample2():Elem %d --> Z=%d",j,pFOILZ[j+MaxNels*i]);
  }
  #endif
  /*Initialize  Sample*/
  *Sample=(foil*)malloc((NFoil)*sizeof(foil));
  if (*Sample==NULL){printf("\n Error allocating memory (Sample)\n");exit(1);}
  for (*MaxZinsample=0,i = 0; i < NFoil ; i++) {

    //obtain the number of elements for this layer
    nelem=pNels[i];
    if(nelem<1){fprintf(stderr,"\n ERROR: At least 1 element in each foil needed\n");exit(1);}

    pfoil=&((*Sample)[i]);
	  pfoil->nfoilelm=nelem;
	  pfoil->thick=(double)pFOILTHICK[i];
	  c=&pfoil->comp;

    c->nelem=nelem;
    if(!(c->elem=(ELEMENT*)calloc(nelem,sizeof(ELEMENT)))){fprintf(stderr,"\n Error allocating memory\n");exit(1);}
    if(!(c->X=(double*)calloc(nelem,sizeof(double)))){fprintf(stderr,"\n Error allocating memory\n");exit(1);}
    if(!(c->xn=(double*)calloc(nelem,sizeof(double)))){fprintf(stderr,"\n Error allocating memory\n");exit(1);}
    if(!(c->w=(double*)calloc(nelem,sizeof(double)))){fprintf(stderr,"\n Error allocating memory\n");exit(1);}
    for(j=0, c->sumX=0., sumM=0. ; j<nelem ; j++){
        c->X[j]= (double) (pAMNT[j+MaxNels*i]);  //NOTE!!: The indexes are switched and "offseted" to deal with the the Fortran convention!!
	    c->elem[j].Z=pFOILZ[j+MaxNels*i];
        c->elem[j].A=Z2mass(c->elem[j].Z, &c->elem[j].IM, 'm');
        c->elem[j].M=(double)pFOILM[j+MaxNels*i];  //The mass passed by NDF is put on the "natural mass"
        c->sumX+=c->X[j];
        sumM+=c->elem[j].M*c->X[j];
        if(c->elem[j].Z>*MaxZinsample) *MaxZinsample=c->elem[j].Z;
    }
    /*Normalize concentrations and calculate the MASS concentration*/
    for(j=0;j<nelem;j++){
      c->xn[j]=c->X[j]/c->sumX;
      c->w[j]=c->X[j]*c->elem[j].M/sumM;
    }
  }
  #if CPIXEVERBOSITY >3
  for(i=0;i<NFoil;i++){
    printf("\nFoil %d: th=%le nelem=%d",i,(*Sample)[i].thick,(*Sample)[i].nfoilelm);
    for(j=0;j<(*Sample)[i].nfoilelm;j++){
      printf("\nTrans: (i=%d , j=%d) (*Sample)[i].comp.elem[j].Z=%d ",i,j,(*Sample)[i].comp.elem[j].Z);
	  printf("\nTrans:             (*Sample)[i].comp.elem[j].M=%lf ",(*Sample)[i].comp.elem[j].M);
	  printf("\nTrans:             (*Sample)[i].comp.elem[j].IM=%lf ",(*Sample)[i].comp.elem[j].IM);
	  printf("\nTrans:             (*Sample)[i].comp.elem[j].A=%d ",(*Sample)[i].comp.elem[j].A);
 	  printf("\nTrans:      X=%lf   xn=%lf   w=%lf",(*Sample)[i].comp.X[j],(*Sample)[i].comp.xn[j],(*Sample)[i].comp.w[j]);
	  }
  }
  #endif
  return(0);


}


/*Copies the results of XYldSums to the array pRESULTS */
int translateresults(int dimSums, int dimResults, const int *PresentElems, const CalibYld *XYldSums, CPIXERESULTS *pRESULTS)
{
  int Z,ielem;
  for(ielem=0, Z=1 ; Z < dimSums ; Z++){
    if(PresentElems[Z]){
      ielem++;
      if(ielem>dimResults){fprintf(stderr,"\n Error: cpixefif: 'dimResults' too small\n");exit(1);}
      pRESULTS[ielem].simareas=XYldSums[Z];
      pRESULTS[ielem].atnum=Z;
    }
  }
  return(0);
}


int translateresultsNDF(int Nels, const int *pFOILZ, const CalibYld *XYldSums, CPIXERESULTS *pRESULTS)
{
  int i,Z;
  for(i=0;i<Nels;i++){
    Z=pFOILZ[i];
    pRESULTS[i].atnum=Z;
    pRESULTS[i].simareas=XYldSums[Z];  ///IMPORTANT!!!!!!: Each isotope gets the FULL number of counts for the element!!!
  }
  return(0);
}


char* mystrsep(char** stringp, const char* delim)
{
  char* start = *stringp;
  char* p;

  p = (start != NULL) ? strpbrk(start, delim) : NULL;

  if (p == NULL){
    *stringp = NULL;
  }
  else{
    *p = '\0';
    *stringp = p + 1;
  }

  return start;
}


void TESTFIF()
{
  printf("\n testfif() is working...\n");
}

void CPIXEMAIN(int NCALL)
{
  char *calibfilenames, *tofree, *fname;
  STATFILENAME fckfilename="";
  STATFILENAME abscoeffilename="";
  STATFILENAME xenerfilename="";
  STATFILENAME dbpath="./";


  EXP_PARAM *pExpPar;
  SIM_PARAM *pSimPar;

  foil *Sample;
  int NFoil, NFoilUsed, dimSums;
  LYR *lyr;
  CalibYld *XYldSums;
  int i,j,l,det;

  /*Static variables (Those that should be maintained between calls of CPIXEMAIN()*/
  static int *PresentElems;
  //static XrayYield *XYldArray;
  static CalibYld *LinesEnerArray;
  static CalibYld **EffArray;   // pointer to be switched from various efficiency arrays
  static FluorCKCoef *FCKCoefArray;
  static AbsCoef *TotAbsCoefArray;
  static SPT *SPTArray;
  static int lastNcall=-1;
  static FILTER Filter;
  static int NDetectors;

  /*Extern Variables (defined in the CPIXEIF Fortran module)*/
  extern EXP_PARAM CPIXEIF_mp_EXPPAR;
  extern char CPIXEIF_mp_CALFILENM[2048];
  extern char CPIXEIF_mp_FLTFILENM[256];
  extern char CPIXEIF_mp_FCKFILENM[256];
  extern char CPIXEIF_mp_ABSFILENM[256];
  extern char CPIXEIF_mp_XENFILENM[256];
  extern char CPIXEIF_mp_DBPATH[256];
  extern int CPIXEIF_mp_CPNLAY;
  extern int CPIXEIF_mp_CPNELS;
  extern int CPIXEIF_mp_DIMRESULTS;
  extern int CPIXEIF_mp_NLAYFILTER;
  extern int CPIXEIF_mp_MAXNELSFILTER;
  extern int CPIXEIF_mp_FILTERCHANGES;

  extern int CPIXEIF_mp_POINT_FOILZ, CPIXEIF_mp_POINT_FILTERZ,
             CPIXEIF_mp_POINT_FILTERNELS;     //these are DEC pointers to ints
  extern int CPIXEIF_mp_POINT_FOILTHICK , CPIXEIF_mp_POINT_FOILM,
             CPIXEIF_mp_POINT_FOILAMNT , CPIXEIF_mp_POINT_FILTERM,
             CPIXEIF_mp_POINT_FILTERTHICK,CPIXEIF_mp_POINT_FILTERAMNT ;  //these are DEC pointers to floats
  extern int CPIXEIF_mp_POINT_PIXERESULTS; //This is a DEC pointer to a CPIXERESULT
  extern int CPIXEIF_mp_POINT_PIXECALC;

  int *pFOILZ, *pFILTERZ, *pFILTERNELS; //Those are the "true" C pointers to ints
  float *pFOILTHICK ,*pFOILM ,*pAMNT,*pFILTERTHICK ,*pFILTERM ,*pFILTERAMNT;
  CPIXERESULTS *pRESULTS;
  CPIXERESULTS *pCALCFLAGS;

  #if CPIXEVERBOSITY > 0
  printf("\n\n\n************ BEGIN OF CPIXE OUTPUT ****************\n\n");
  #endif

  //Transforming fortran (DEC) pointers (which are integers) into true C pointers
  pFOILZ=(int*)CPIXEIF_mp_POINT_FOILZ;
  pFOILTHICK=(float*)CPIXEIF_mp_POINT_FOILTHICK;
  pFOILM=(float*)CPIXEIF_mp_POINT_FOILM;
  pAMNT=(float*)CPIXEIF_mp_POINT_FOILAMNT;
  pFILTERNELS=(int*)CPIXEIF_mp_POINT_FILTERNELS;
  pFILTERZ=(int*)CPIXEIF_mp_POINT_FILTERZ;
  pFILTERTHICK=(float*)CPIXEIF_mp_POINT_FILTERTHICK;
  pFILTERM=(float*)CPIXEIF_mp_POINT_FILTERM;
  pFILTERAMNT=(float*)CPIXEIF_mp_POINT_FILTERAMNT;
  pRESULTS=(CPIXERESULTS*) CPIXEIF_mp_POINT_PIXERESULTS;
  pCALCFLAGS=(CPIXERESULTS*)CPIXEIF_mp_POINT_PIXECALC;

  /*Note: The following initialization needs to be done each time the function is called*/
  Sample=NULL;
  lyr=NULL;

  //make a convenience pointer to the EXPPAR and SIMPAR structures defined in the fortran module
  pExpPar=&CPIXEIF_mp_EXPPAR;
  pSimPar=&pExpPar->simpar;

  if(NCALL>0){
    if(lastNcall<0){fprintf(stderr,"\n ERROR: CPIXEMAIN: CPIXEMAIN was not initialized.\n");exit(1);}
    /******* BEGIN CALCULATION*********/
    /*
    These calls are all what is needed to do for each iteration
    (But don't forget also the call to freeReusable() before starting a new iteration)
    */

    //translate the sample definition made in NDF to that used in CPIXE
    NFoil=CPIXEIF_mp_CPNLAY;
    translatesample(NFoil,CPIXEIF_mp_CPNELS,pFOILZ,pFOILM,pAMNT,pFOILTHICK,&pSimPar->MaxZinsample,&Sample);

  //translate Filter definition
    if(Filter.changes){
      Filter.nlyr=CPIXEIF_mp_NLAYFILTER;
      Filter.geomcorr=1; //filter must be perpendicular
      translatesample2(Filter.nlyr, CPIXEIF_mp_MAXNELSFILTER , pFILTERNELS, pFILTERZ,
                        pFILTERM, pFILTERAMNT, pFILTERTHICK,
              					&Filter.MaxZ, &Filter.foil );
      createPresentElems(Filter.MaxZ, Filter.nlyr, Filter.foil, &Filter.FilterElems);
      //calculates the transmission values for the filter
      FilterTrans(pExpPar->simpar.MaxZinsample, LinesEnerArray, TotAbsCoefArray, PresentElems, &Filter);
      //the Filter.changes variable is set externally
      Filter.changes=CPIXEIF_mp_FILTERCHANGES;
    }

    //Calculate the fluence (note that in cpixe this is calculated at readinput() )
    //IMPORTANT: Note that NDF introduces the DTCC factor implicitly in ColCharge and hardcodes DTCC=1
    pExpPar->Fluence=pExpPar->simpar.ColCharge*pExpPar->simpar.DTCC/(pExpPar->ioncharge*ELECTRONCHARGE_IN_UC);

    initlyrarray(pExpPar, Sample, LinesEnerArray, TotAbsCoefArray, SPTArray, PresentElems, NFoil, &NFoilUsed, &lyr);
    dimSums=pSimPar->MaxZinsample+1;
    XYldSums=(CalibYld*)calloc(dimSums,sizeof(CalibYld));  //Note that XYldSums is 0-initialized
    integrate_Simpson2(pExpPar, TotAbsCoefArray, FCKCoefArray, EffArray[pExpPar->DetIdx] ,NFoilUsed, &Filter, lyr, XYldSums);
    translateresultsNDF(CPIXEIF_mp_CPNELS, pFOILZ, XYldSums, pRESULTS);
      /******END CALCULATION**********/


    #if CPIXEVERBOSITY > 0
    /**********BEGIN DEBUG LINES*******/
    /* Just write stuff for debugging*/
    printf("\n\nDEBUG:  Ebeam=%lf IncAng=%lf  DetAng=%lf \n",
          pExpPar->BeamEner, pExpPar->IncAng/DEG2RAD, pExpPar->DetAng/DEG2RAD);
    printf("\nDEBUG:  Charge=%lf   DTCC=%lf DetColFac=%lf\n",
          pExpPar->simpar.ColCharge, pExpPar->simpar.DTCC, pExpPar->DetColFac);

    printf("\n\nDEBUG: Iteration %d\n",NCALL);

    for(i=0;i<NFoil;i++){
      printf("\Sample[%d]  (%d elem)\n",i,Sample[i].nfoilelm);
      printf("\n---Eff. thickness=%.3le  (%d sublyrs)\n",lyr[i+1].ThickIn,lyr[i+1].FESxlen);

      for(j=0;j<Sample[i].comp.nelem;j++){
      printf("'%s'(Z=%d) X[%d]=%lf (%lfat%% / %lfM%%)\n",
          ChemicalSymbol[Sample[i].comp.elem[j].Z],Sample[i].comp.elem[j].Z,j,Sample[i].comp.X[j],100*Sample[i].comp.xn[j],100*Sample[i].comp.w[j]);
      }
      printf("\nFinal Energy in this layer=%lf keV\n\n",lyr[i+1].FoilOutEner);
    }

    for(i=0;i<Filter.nlyr;i++){
      printf("\nFilter[%d] (%lf 1e15at/cm2)  (%d elem)  \n",i,Filter.foil[i].thick,Filter.foil[i].nfoilelm);
      for(j=0;j<Filter.foil[i].comp.nelem;j++){
      printf("(Z=%d) X[%d]=%lf (%lfat%% / %lfM%%)\n",
          Filter.foil[i].comp.elem[j].Z,j,Filter.foil[i].comp.X[j],100*Filter.foil[i].comp.xn[j],100*Filter.foil[i].comp.w[j]);
      }
    }
    for(i=0;i<dimSums;i++){
      if(i<=pSimPar->MaxZinsample && PresentElems[i]){
        printf("\n\n ************RESULTS for %s (Z=%d)\n",ChemicalSymbol[Sample[i].comp.elem[j].Z],i);
        /*K lines:*/
        for (j = 1; j <= 3; j++)
          if(XYldSums[i].K_[j] > 0.)
            printf("\n%10s (%2.2lfkeV)\t%le counts   (cal=%.3le Flt=%.2le)", LineNm[1][j], LinesEnerArray[i].K_[j], XYldSums[i].K_[j],EffArray[pExpPar->DetIdx][i].K_[j],Filter.Trans[i].K_[j]);
        /*L-Lines*/
        for (j = 1; j <= 3; j++)
          for (l = 1; l <= 3; l++)
            if(XYldSums[i].L_[j][l] > 0.)
              printf("\n%10s (%2.2lfkeV)\t%le counts   (cal=%.3le Flt=%.2le)", LineNm[j+1][l], LinesEnerArray[i].L_[j][l], XYldSums[i].L_[j][l],EffArray[pExpPar->DetIdx][i].L_[j][l],Filter.Trans[i].L_[j][l]);
        /*M lines:*/
        for (j = 1; j <= 3; j++)
          if(XYldSums[i].M_[j] > 0.)
            printf("\n%10s (%2.2lfkeV)\t%le counts   (cal=%.3le Flt=%.2le)",LineNm[5][j], LinesEnerArray[i].M_[j], XYldSums[i].M_[j],EffArray[pExpPar->DetIdx][i].M_[j],Filter.Trans[i].M_[j]);
      }
    }
//    getchar();
    /**********END DEBUG LINES*******/
    #endif

    /*It is VERY important to call freeReusable() before starting a new iteration*/
    freeReusable(NFoil,&lyr,&Sample,&XYldSums);

    if(Filter.changes)freeFilter(&Filter);

  }

  else if(NCALL==0){
    /**********BEGIN INITIALIZATION*********/
    /*
    This Block is run only when initialization is required.
    It reads the databases (Fluorescence, Stoppings,...) and performs some operations on
    data that never change within a fit.
    */
    
    tofree = calibfilenames = malloc(strlen(CPIXEIF_mp_CALFILENM) + 1);
    strcpy(calibfilenames, CPIXEIF_mp_CALFILENM);
    strcpy(dbpath,CPIXEIF_mp_DBPATH);
    strcpy(fckfilename  ,CPIXEIF_mp_FCKFILENM);
    strcpy(abscoeffilename,CPIXEIF_mp_ABSFILENM);
    strcpy(xenerfilename,CPIXEIF_mp_XENFILENM);

    #if CPIXEVERBOSITY > 0
    printf("\nDEBUG: Initializing. Reading databases...\n\n");
    printf("\nDEBUG: Stopping Coefs DB: '%sSCOEF.95[A/B]'",dbpath);
    printf("\nDEBUG: Absorption Coefs DB: '%s'",abscoeffilename);
    printf("\nDEBUG: Fluorescence Coefs DB: '%s'",fckfilename);
    printf("\nDEBUG: X-ray Energies DB: '%s'\n",xenerfilename);
    printf("\nDEBUG: Detector Calibration DB: '%s'\n",calibfilenames);
    #endif

    //translate the sample definition made in NDF to that used in CPIXE
    NFoil=CPIXEIF_mp_CPNLAY;
    translatesample(NFoil,CPIXEIF_mp_CPNELS,pFOILZ,pFOILM,pAMNT,pFOILTHICK,&pSimPar->MaxZinsample,&Sample);

    //translate Filter definition
    Filter.nlyr=CPIXEIF_mp_NLAYFILTER;
    translatesample2(Filter.nlyr, CPIXEIF_mp_MAXNELSFILTER , pFILTERNELS, pFILTERZ,
                      pFILTERM, pFILTERAMNT, pFILTERTHICK,
            					&Filter.MaxZ, &Filter.foil );
    createPresentElems(Filter.MaxZ, Filter.nlyr, Filter.foil, &Filter.FilterElems);
    Filter.changes=1;    //First time at least Filter should be read:

    createPresentElems(pSimPar->MaxZinsample, NFoil, Sample, &PresentElems);

    //Read the energies for the lines used
    readLinesEner(xenerfilename, PresentElems, pSimPar->MaxZinsample, &LinesEnerArray);

    readFCK(fckfilename, pSimPar->MaxZinsample,&FCKCoefArray);
    readAbsCoef(abscoeffilename, pSimPar->MaxZinsample, &TotAbsCoefArray);
    createSPTs(dbpath,pExpPar, pSimPar->MaxZinsample, PresentElems, (double)(2*pExpPar->ion.A), &SPTArray);
    
    // Determine the number of detectors (1 + number occurrences of ';' in calibfilenames)
    for (i=0, NDetectors=1; calibfilenames[i]; i++)
        NDetectors += (calibfilenames[i] == ';');
    
    // initialize the array of pointers to the calibration efficiencies
    EffArray = malloc(NDetectors * sizeof(CalibYld *));
    
    // split the calibfilenames on ";" and read the efficiencies from the calibration file of each detector 
    for (det=0; (fname = mystrsep(&calibfilenames, ";")) != NULL; det++){
        readEff3(fname, PresentElems, pSimPar->MaxZinsample, LinesEnerArray, &(EffArray[det]));
        #if CPIXEVERBOSITY > 0
        printf("\nDEBUG: \tDetector %d: %s\n", det, fname);
        #endif
    }
    
    free(tofree);
    safefree((void*)&Sample); //Clear the initialized Matrix Array.
    safefree((void*)&Filter.foil); //Clear the initialized Filter Foil.
    /********** END INITIALIZATION *********/
  }

  else if(NCALL<0){
    /******** BEGIN TIDING UP BEFORE FINAL EXIT*********/
    /*Free everything for a clean exit*/
    #if CPIXEVERBOSITY > 0
    printf("\nDEBUG: Cleaning CPIXE Memory usage...\n\n");
    #endif

    //Note: Filter.changes=0 implies that freeFilter was not called in last iteration:
    if(!Filter.changes)freeFilter(&Filter);

    for(i=1; i<=pSimPar->MaxZinsample ;i++){
      if(PresentElems[i]){
        free(SPTArray[i].S);
        free(SPTArray[i].E);
        free(SPTArray[i].dSdE);
      }
    }
    free(SPTArray);
    free(PresentElems);
    //free(XYldArray);
    free(LinesEnerArray);
    for (det=0; det<NDetectors; det++){
        free(EffArray[det]);
    }
    free(EffArray);
    free(FCKCoefArray);
    free(TotAbsCoefArray);
    free(XYldSums);
    /******** END TIDING UP BEFORE FINAL EXIT*********/
  }

//  printf("\n\nDEBUG: End of program reached\n\n");getchar();

  /*Store Number of Ncall*/
  lastNcall=NCALL;
  #if CPIXEVERBOSITY > 0
  printf("\n\n\n************ END OF CPIXE OUTPUT ****************\n\n");
  #endif

  return;
}
