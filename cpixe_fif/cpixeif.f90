!**********************************************************************!
!
! CPIXEIF.f90
!
! (c) Carlos Pascual Izarra.  All rights reserved. 
!	   carlos.pascual@uam.es 
!
! CPIXEIF is a Fortran module which acts as a compatibility layer between
! a fortran program and the cpixe library written in C
!
!
!
!
!Note the following issues concerning Fortran-C interfacing:
!
!	1.-Character strings :
!		1.1.-Even when character strings are defined, no info should be passed in them to avoid 
!			problems with the convention for string termination in C ('0').
!			(some character strings are defined as part of derived types in order to match
!			the equivalent C structures, but they are not meant to be used)
!			
!		NOTE: if it is absolutely necessary to pass a string, use fstring2cstring() 
!			to convert before the C part accesses to the string.
!
!
!	2.-Arrays:
!		2.1.-Vectors and other arrays must be defined starting in 0 like in C
!
!		2.2.-Multidimensional arrays have the dimensional indexes "flipped":
!			
!		example: the element array[i][j] in C is referenced in fortran as array(j,i).
!				 
!		example2: Declaration in C:       int array[N][M]  
!				  Declaration in Fortran: integer array(0:M-1, 0:N-1)  
!
!
!	3.-Any C function that needs to be called must be declared within an INTERFACE block, 
!	  using the "DEC$ ATTRIBUTES C" and the "ALIAS"
!
!   
!
!
!*************************************************************************!

module CPIXEIF

implicit none


!------------------------------------
!interface declarations. (needed to call to C functions)
!
!Note that all the lines containing the "DEC$" instruction
! are potentially non portable (compiler dependent) 

INTERFACE
subroutine CPIXEMAIN(NCALL)
!DEC$ ATTRIBUTES C, ALIAS:'_CPIXEMAIN' :: CPIXEMAIN
integer NCALL
end subroutine CPIXEMAIN
END INTERFACE

!------------------------------------
!Type definitions (these mimic some C structures used in cpixe) 
!

!Structure for defining elements
type ELEMENT
  character(len=3) symbol  !Chemical symbol (this is better not to use for passing info to C
  integer Z;            !Atomic number
  integer A;			!Number of nucleons
  real(8) M;			!Natural Atomic weight, in amu 
  real(8) IM;			!Isotopic mass, in amu
end Type ELEMENT

!type defining the detector filter properties
type ABSORBER
  integer number
  character(len=6) name
  integer nreg
  real(8) ereg(0:10)
  real(8) coef(0:2,0:10) !VERY IMPORTANT: note that indexes are interchanged with respect to C
  real(8) h(0:2)
end type ABSORBER

!type defining more experimental parameters (stupidily called "simulation" parameters)
type SIM_PARAM
  integer MaxZinsample      !Maximum atomic number present in sample
  integer AllowSXFCorr		!Flag for secondary fluorescence calculation.1=true, 0=false
  integer AllowXEqCalc		!Flag for X-ray equivalent calculation. 1=true, 0=false
  real(8) DTCC				!Dead Time Correction coefficient
  !real(8) DTCCerr			!Error in Dead Time Correction coefficient (Not really used)
  real(8) ColCharge 		!Collected charge (fluence*charge_state), in uC.
  real(8) CalEner			!Energy at which the calibration was done.
  integer useFilter			!Flag: whether a filter on the detector is used (1) or not (0).
  type (ABSORBER):: Filter	!Filter definition
end type SIM_PARAM


!type defining some experimental parameters
type EXP_PARAM
  type (ELEMENT):: ion		!Beam type @todo For the moment only H ions are supported.
  real(8) ioncharge			!Beam ion charge (useful only for ions heavier than H)
  real(8) BeamEner			!Beam Energy, in keV
  real(8) BeamCol			!colimator aperture (diameter of the beam), in mm
  real(8) DetColFac			!Relative diameter of det colimator to that used during calib
  real(8) IncAng			!Incident angle (beam to surface normal), in degrees 
  real(8) DetAng			!Detector Angle (detector to surface normal), in degrees
  real(8) cosDet			!For optimization. Cos(IncDet)
  real(8) cosInc			!For optimization. Cos(IncAng)
  real(8) cosFac			!For optimization. Cos(IncAng)/Cos(IncDet)
  real(8) BeamCross			!Beam cross section, in cm2
  real(8) FinalEner			!Minimum energy considered, in keV
  real(8) SAngFract			!Solid Angle fraction of the detector (solid angle in steradians/4PI)
  real(8) Fluence			!Number of beam particles (corrected by dead time)
  integer DetIdx			!Detector number (note that first detector is =0 !)
  type (SIM_PARAM)::SIMPAR
end type EXP_PARAM

!L,K & M lines intensities
type CALIBYLD 
  real(8) K_(0:3)		! K Lines 
  real(8) L_(0:3,0:3)	! 1-LIII , 2-LII , 3-LI   CAUTION: indexes are interchanged with respect to C
  real(8)  M_(0:3)		! M  Lines
end type CALIBYLD

type CPIXERESULTS
  integer atnum
  type(CALIBYLD)::simareas
  type(CALIBYLD)::err
end type CPIXERESULTS

!------------------------------------
!data declaration.
!Here some variables that are used are declared 
!(they can be accessed from the C code too using the extern declaration) 
!

type (EXP_PARAM) :: EXPPAR
!type (SIM_PARAM) :: SIMPAR

character(len=2048) CALFILENM   !len =  8*256
character(len=256) FCKFILENM
character(len=256) ABSFILENM
character(len=256) XENFILENM
character(len=256) FLTFILENM
character(len=256) DBPATH

integer CPNLAY
integer CPNELS
integer DIMRESULTS
integer NLAYFILTER
integer MAXNELSFILTER
integer FILTERCHANGES

!pointers:
integer point_FOILZ ,point_FOILTHICK ,point_FOILM ,point_FOILAMNT ,point_PIXERESULTS
integer point_FILTERNELS,point_FILTERZ,point_FILTERM,point_FILTERTHICK,point_FILTERAMNT
integer point_PIXECALC


type(CPIXERESULTS),allocatable :: PIXERESULTS(:)
type(CPIXERESULTS),allocatable :: PIXECALC(:)
type(CPIXERESULTS),allocatable :: PIXERESULTS_nlay(:,:,:)
type(CPIXERESULTS),allocatable :: PIXERESULTS_total(:,:,:)
type(CPIXERESULTS),allocatable :: b_ijk_PIXElay(:,:,:),b_ijk_PIXElay_tot(:,:,:),b_ijk_PIXElay_loop(:,:,:)
type(CPIXERESULTS),allocatable :: b_ijk_PIXEtotal(:,:),b_ijk_PIXEtotal_tot(:,:),b_ijk_PIXEtotal_loop(:,:)
contains
!------------------------------------
!Functions and routines useful for the interface:




!Function for conversion of character strings from fortran to C
!Inputs:
!  fstring: a normal fortran string
!Outputs:
!  same as fstring but trailing blanks are eliminated and it is null-terminated
subroutine fstring2cstring(fstring,cstring)
	character(len=*), intent(in)  :: fstring
	character(len=*), intent(out) :: cstring
	cstring=trim(fstring)//char(0)
end subroutine fstring2cstring

!function for dummy test the interface.
subroutine testcpixe()
  integer i,j
  
	
	!BEGIN DUMMY INPUT DATA
	call fstring2cstring('.\testdata\calib.in;.\testdata\.\.\calib.in',CALFILENM)
	call fstring2cstring('.\testdata\SC_PRA74.fck',FCKFILENM)
	call fstring2cstring('.\testdata\xabs.abs',ABSFILENM)
	call fstring2cstring('.\testdata\filter.in',FLTFILENM)  

	call fstring2cstring('H ',EXPPAR%ion%symbol)
	EXPPAR%ion%Z=1
	EXPPAR%ion%A=1
	EXPPAR%ion%M=1.008
	EXPPAR%ion%IM=1.008
	EXPPAR%BeamEner=2300.			!Beam Energy, in keV
	EXPPAR%BeamCol=1			!colimator aperture (diameter of the beam), in mm
	EXPPAR%DetColFac=1			!Relative diameter of det colimator to that used during calib
	EXPPAR%IncAng=7.5			!Incident angle (beam to surface normal), in degrees 
	EXPPAR%DetAng=77.5			!Detector Angle (detector to surface normal), in degrees
	EXPPAR%cosDet=cosd(EXPPAR%DetAng)			!For optimization. Cos(IncDet)
	EXPPAR%cosInc=cosd(EXPPAR%IncAng)			!For optimization. Cos(IncAng)
	EXPPAR%cosFac=EXPPAR%cosInc/EXPPAR%cosDet			!For optimization. Cos(IncAng)/Cos(IncDet)
	EXPPAR%BeamCross=EXPPAR%BeamCol*EXPPAR%BeamCol*3.14156/400.			!Beam cross section, in cm2
	EXPPAR%FinalEner=min(100.,0.1*EXPPAR%BeamEner)			!Minimum energy considered, in keV
	EXPPAR%SIMPAR%AllowSXFCorr=0	!Flag for secondary fluorescence calculation.1=true, 0=false
	EXPPAR%SIMPAR%AllowXEqCalc=0	!Flag for X-ray equivalent calculation. 1=true, 0=false
	EXPPAR%SIMPAR%DTCC=0.91667	!Dead Time Correction coefficient
	EXPPAR%SIMPAR%ColCharge=100 	!Collected charge (fluence*charge_state), in uC.
	!EXPPAR%SIMPAR%CalEner=		!Energy at which the calibration was done. (It is read from CALFILE)
	EXPPAR%SIMPAR%useFilter=0		!Flag: whether a filter on the detector is used (1) or not (0).
	EXPPAR%DetIdx=0		! Use 1st detector 
  
	!Filters are still not supported. Some dummy data is written here for test 
	!but in the real case, the filter coefs will be read from a file 
	do i=0,10
		do j=0,2
			EXPPAR%SIMPAR%Filter%coef(j,i)=i+j*.1
		enddo
	enddo
	EXPPAR%SIMPAR%Filter%h(0)=0.1
	EXPPAR%SIMPAR%Filter%h(1)=1.1
	EXPPAR%SIMPAR%Filter%h(2)=2.1

	!sample definition
	!Here I select a way of defining the sample which may not be the way used in NDF.
	!CPIXE has its own way of defining the sample and thus a "translation routine" is used when 
	!calling to cpixemain. Hence, that translation routine will need to be revised to adapt
	!to the real scheme of sample definition used in NDF

	!I assume the sample being composed by "NFOIL" foils, with at most "MAXNELEM" elements
	!on them, each one defined by its atomic number, its weight and its atomic abundancy.
	!For each foil, nelem is defined


!	NFOIL=3
!	allocate(FOILTHICK(NFOIL))
!	if (.not.allocated(FOILTHICK)) stop 'Memory allocation problem'
!	point_FOILTHICK=loc(FOILTHICK)

	
!	FOILTHICK= (/10.,50.,1e6/)  
!	NELEM=(/2,3,3/) 
!	!NELEM=(/1,3,3/) 
!	MAXNELEM=maxval(NELEM)


!	allocate(FOILZ(NFOIL,MAXNELEM))
!	if (.not.allocated(FOILZ)) stop 'Memory allocation problem'
!	point_FOILZ=loc(FOILZ)

!	allocate(FOILM(NFOIL,MAXNELEM))
!	if (.not.allocated(FOILM)) stop 'Memory allocation problem'
!	point_FOILM=loc(FOILM)

!	allocate(FOILABUN(NFOIL,MAXNELEM))
!	if (.not.allocated(FOILABUN)) stop 'Memory allocation problem'
!	point_FOILABUN=loc(FOILABUN)

!	FOILZ(1,:)=(/14,14/)
!	FOILZ(2,:)=(/31,49,51/)
!	FOILZ(3,:)=(/31,51,51/)

!	FOILM(1,:)=(/28.086,28.086/)
!	FOILM(2,:)=(/69.72 , 114.82, 121.75/)
!	FOILM(3,:)=(/69.72 ,  121.75,  121.75/)

!	FOILABUN(1,:)=(/4.,4./)
!	FOILABUN(2,:)=(/4. , 3. , 5./)
!	FOILABUN(3,:)=(/1. , .5, .5/)

	!write(*,*)'NDF: FOILTHICK->',point_FOILTHICK,'    NELEM-->',point_NELEM

	DIMRESULTS=4  !This must be equal to (or bigger than) the total number of different elements in the sample
	allocate(PIXERESULTS(DIMRESULTS))
	if (.not.allocated(PIXERESULTS)) stop 'Memory allocation problem'
	point_PIXERESULTS=loc(PIXERESULTS)

	!END DUMMY INPUT DATA

	
!	write(*,*)'ndf: Foil(1,:)=',Foilz(1,:),'   nelem=',NELEM(1)
!    write(*,*)'ndf: Foil(2,:)=',Foilz(2,:),'   nelem=',NELEM(2)

	


	call cpixemain(0)

	write(*,*)PIXERESULTS

!do i=1,DIMRESULTS
  

!    if(i<=pSimPar->MaxZinsample && PresentElems[i]){
!      printf("\n\n ************RESULTS for %s (Z=%d)\n",XYldArray[i].symb,i);
!      /*K lines:*/
!      for (j = 1; j <= 3; j++) 
!        if(XYldSums[i].K_[j] > 0.) 
!          printf("\n%10s (%2.2lfkeV)\t%le counts   (calib=%le)", LineNm[1][j], XYldArray[i].ener.K_[j], XYldSums[i].K_[j],XYldArray[i].XYld.K_[j]);
!      /*L-Lines*/
!      for (j = 1; j <= 3; j++)
!        for (l = 1; l <= 3; l++) 
!          if(XYldSums[i].L_[j][l] > 0.)
!            printf("\n%10s (%2.2lfkeV)\t%le counts   (calib=%le)", LineNm[j+1][l], XYldArray[i].ener.L_[j][l], XYldSums[i].L_[j][l],XYldArray[i].XYld.L_[j][l]); 
!      /*M lines:*/
!      for (j = 1; j <= 3; j++) 
!        if(XYldSums[i].M_[j] > 0.) 
!          printf("\n%10s (%2.2lfkeV)\t%le counts   (calib=%le)",LineNm[5][j], XYldArray[i].ener.M_[j], XYldSums[i].M_[j],XYldArray[i].XYld.M_[j]);
!    }
!  enddo


end subroutine testcpixe

end module CPIXEIF
